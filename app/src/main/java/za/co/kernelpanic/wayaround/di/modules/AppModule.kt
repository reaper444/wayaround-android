package za.co.kernelpanic.wayaround.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import za.co.kernelpanic.wayaround.repository.WayAroundRepository
import za.co.kernelpanic.wayaround.repository.WayAroundRepositoryImpl
import za.co.kernelpanic.wayaround.repository.remote.NetworkDataSource
import za.co.kernelpanic.wayaround.repository.remote.NetworkDataSourceImpl
import za.co.kernelpanic.wayaround.repository.remote.WayAroundApi
import javax.inject.Singleton

@Module
class AppModule {

    var application = Application()

    private fun getSharedPreferenceString(): String {
        return "WayAroundPrefs"
    }

    @Provides
    fun providesApplicationContext(app: Application): Context {
        application = app
        return application
    }

    @Provides
    @Singleton
    fun providesNetworkDataSource(wayAroundApi: WayAroundApi): NetworkDataSource {
        return NetworkDataSourceImpl(wayAroundApi)
    }

    @Provides
    @Singleton
    fun wayAroundRepository(networkDataSource: NetworkDataSourceImpl): WayAroundRepository {
        return WayAroundRepositoryImpl(networkDataSource)
    }
}
