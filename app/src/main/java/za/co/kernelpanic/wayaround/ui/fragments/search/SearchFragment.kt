package za.co.kernelpanic.wayaround.ui.fragments.search

import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import javax.inject.Inject

import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import za.co.kernelpanic.wayaround.R
import za.co.kernelpanic.wayaround.di.modules.WayAroundViewModelFactory

class SearchFragment : DaggerFragment() {

    @Inject
    lateinit var wayAroundViewModelFactory: WayAroundViewModelFactory

    @Inject
    lateinit var searchViewModel: SearchViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchViewModel = ViewModelProviders.of(this, wayAroundViewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object {

        private val LOG_TAG = SearchFragment::class.java.simpleName

        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }
}// Required empty public constructor
