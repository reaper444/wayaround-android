package za.co.kernelpanic.wayaround.util

class WayAroundConstants {

    companion object {
        const val METROPOLITAN: String = "metropolitan"
        const val PICCADILLY: String = "piccadilly"
        const val NORTHERN: String = "northern"
        const val HAMMERSMITH_CITY = "hammersmith-city"
        const val WATERLOO: String = "waterloo-city"
        const val CENTRAL: String = "central"
        const val CIRCLE: String = "circle"
        const val BAKERLOO: String = "bakerloo"
        const val JUBILEE: String = "jubilee"
        const val DISTRICT: String = "district"
        const val VICTORIA: String = "victoria"
    }
}