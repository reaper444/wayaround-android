package za.co.kernelpanic.wayaround.repository.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats;

public interface WayAroundApi {

    @GET("all/status")
    Call<TrainStats> getStatusInfo();
}
