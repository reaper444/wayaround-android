package za.co.kernelpanic.wayaround.repository.remote;

import androidx.lifecycle.LiveData;

import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats;

public interface NetworkDataSource {

    LiveData<TrainStats> getStatus();
}
