package za.co.kernelpanic.wayaround.repository;

import androidx.lifecycle.LiveData;

import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats;

public interface WayAroundRepository {

    LiveData<TrainStats> getTrainStats();
}
