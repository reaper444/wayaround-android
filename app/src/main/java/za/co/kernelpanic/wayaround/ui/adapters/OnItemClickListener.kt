package za.co.kernelpanic.wayaround.ui.adapters

interface OnItemClickListener {

    fun onClick()
}