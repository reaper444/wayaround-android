package za.co.kernelpanic.wayaround.ui.fragments.status;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats;
import za.co.kernelpanic.wayaround.repository.WayAroundRepositoryImpl;

@Singleton
public class StatusViewModel extends ViewModel {

    private WayAroundRepositoryImpl wayAroundRepository;

    @Inject
    public StatusViewModel(WayAroundRepositoryImpl wayAroundRepository) {
        this.wayAroundRepository = wayAroundRepository;
    }

    public LiveData<TrainStats> getTrainData() {
        return wayAroundRepository.getTrainStats();
    }
}
