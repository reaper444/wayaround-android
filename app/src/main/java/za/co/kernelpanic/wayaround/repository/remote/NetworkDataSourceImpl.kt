package za.co.kernelpanic.wayaround.repository.remote

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.util.Log

import javax.inject.Inject
import javax.inject.Singleton

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats

@Singleton
class NetworkDataSourceImpl @Inject
constructor(private val wayAroundApi: WayAroundApi) : NetworkDataSource {
    private val trainStatsLiveData: MutableLiveData<TrainStats> = MutableLiveData()

    override fun getStatus(): LiveData<TrainStats> {

        val getStatusInfo = wayAroundApi.statusInfo

        getStatusInfo.enqueue(object : Callback<TrainStats> {
            override fun onResponse(call: Call<TrainStats>, response: Response<TrainStats>) {
                if (response.isSuccessful) {
                    Log.v(LOG_TAG, "response success!")

                    trainStatsLiveData.setValue(response.body())
                } else {

                    Log.e(LOG_TAG, "something went wrong")

                    when (response.code()) {
                        404 -> Log.e(LOG_TAG, "")
                        500 -> Log.e(LOG_TAG, "500, the server is dead")
                        else -> Log.e(LOG_TAG, "i have no idea what happened")
                    }
                }
            }

            override fun onFailure(call: Call<TrainStats>, t: Throwable) {
                Log.e(LOG_TAG, "Error!!!" + t.message + "Caused by: " + t.cause)
            }
        })
        return trainStatsLiveData
    }

    companion object {

        private val LOG_TAG = NetworkDataSourceImpl::class.java.simpleName
    }
}
