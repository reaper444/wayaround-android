package za.co.kernelpanic.wayaround.di.modules

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import za.co.kernelpanic.wayaround.repository.remote.WayAroundApi
import javax.inject.Singleton

@Module
class NetworkModule {

    private fun getBaseUrl(): String {
        return "https://wayaround.kernelpanic.co.za/api/v1/underground/"
    }

    @Provides
    @Singleton
    fun providesOKhttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        val okHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return okHttpClient
    }

    @Provides
    @Singleton
    fun providesJsonConverter(): Moshi {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    @Provides
    @Singleton
    fun providesRetrofitInstance(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .client(providesOKhttpClient())
                .addConverterFactory(MoshiConverterFactory.create(providesJsonConverter()))
                .build()
    }

    @Provides
    @Singleton
    fun providesWayAroundApi(retrofit: Retrofit): WayAroundApi {
        return retrofit.create(WayAroundApi::class.java)
    }
}