package za.co.kernelpanic.wayaround.data.model.london.underground

import com.squareup.moshi.Json

open class BaseData {
    @Json(name = "id")
    var lineId: String? = "someId"

    @Json(name = "name")
    var lineName: String? = "someTrain"

    @Json(name = "severityLevel")
    var severityLevel: String? = "5"

    @Json(name = "severityInfo")
    var severityInfo: String? = "Probably Good Service"
}