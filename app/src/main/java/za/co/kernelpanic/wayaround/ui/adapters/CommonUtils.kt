package za.co.kernelpanic.wayaround.ui.adapters

import za.co.kernelpanic.wayaround.R
import za.co.kernelpanic.wayaround.data.model.london.underground.BaseData
import za.co.kernelpanic.wayaround.data.model.london.underground.CombinedData
import za.co.kernelpanic.wayaround.data.model.london.underground.OnlineRoutes
import za.co.kernelpanic.wayaround.data.model.london.underground.WarningRoutes
import za.co.kernelpanic.wayaround.util.WayAroundConstants

class CommonUtils {

    companion object {

        fun getUndergroundLogo(lineId: String?): Int {

            return when (lineId) {

                WayAroundConstants.BAKERLOO -> R.drawable.london_underground_bakerloo
                WayAroundConstants.CENTRAL -> R.drawable.london_underground_central
                WayAroundConstants.CIRCLE -> R.drawable.london_underground_circle
                WayAroundConstants.DISTRICT -> R.drawable.london_underground_district
                WayAroundConstants.HAMMERSMITH_CITY -> R.drawable.london_underground_hammersmith
                WayAroundConstants.JUBILEE -> R.drawable.london_underground_jubilee
                WayAroundConstants.METROPOLITAN -> R.drawable.london_underground_metropolitan
                WayAroundConstants.NORTHERN -> R.drawable.london_underground_northern
                WayAroundConstants.PICCADILLY -> R.drawable.london_underground_picadilly
                WayAroundConstants.VICTORIA -> R.drawable.london_underground_victoria
                WayAroundConstants.WATERLOO -> R.drawable.london_underground_waterloo
                else -> -1
            }
        }

        fun getStatusIndicator(severityLevel: String?): Int {

            val indicatorValue = severityLevel?.toInt()

            if (indicatorValue != null) {
                if (indicatorValue == 10) {
                    return R.drawable.online_indicator
                }
                if ((indicatorValue < 10) && (indicatorValue >= 5)) {
                    return R.drawable.warning_indicator
                } else if ((indicatorValue < 5) || (indicatorValue == 20)) {
                    return R.drawable.offline_indicator
                }
            }
            return -1
        }

        fun isUndergroundWarningAvailable(warningRoutes: List<WarningRoutes>): Boolean {

            if (warningRoutes.isNotEmpty()) {
                return true
            }
            return false
        }

        fun mergeUndergroundRouteLists(onlineData: List<OnlineRoutes>, warningData: List<WarningRoutes>): List<BaseData> {

            val combinedList = ArrayList<BaseData>()
            combinedList.addAll(onlineData)
            combinedList.addAll(warningData)
            return combinedList
        }
    }
}