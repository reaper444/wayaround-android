package za.co.kernelpanic.wayaround.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import za.co.kernelpanic.wayaround.di.submodules.FragmentBuildersModule
import za.co.kernelpanic.wayaround.ui.activity.BaseActivity
import za.co.kernelpanic.wayaround.ui.activity.UndergroundActivity

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    internal abstract fun contributesUndergroundActivity(): UndergroundActivity

    @ContributesAndroidInjector
    internal abstract fun contributesBaseActiviy(): BaseActivity
}