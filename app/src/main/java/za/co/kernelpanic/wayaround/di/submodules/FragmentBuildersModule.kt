package za.co.kernelpanic.wayaround.di.submodules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import za.co.kernelpanic.wayaround.ui.fragments.history.HistoryFragment
import za.co.kernelpanic.wayaround.ui.fragments.search.SearchFragment
import za.co.kernelpanic.wayaround.ui.fragments.status.StatusFragment

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun historyFragment(): HistoryFragment

    @ContributesAndroidInjector
    internal abstract fun searchFragment(): SearchFragment

    @ContributesAndroidInjector
    internal abstract fun statusFragment(): StatusFragment
}
