package za.co.kernelpanic.wayaround.ui.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log

import javax.inject.Inject

import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import za.co.kernelpanic.wayaround.R

open class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun startFragment(fragment: Fragment, replaceFragment: Boolean, tag: String, addToBackStack: Boolean) {
        startFragment(fragment, R.id.fragmentContainer, replaceFragment, addToBackStack, tag)
    }

    fun startFragment(fragment: Fragment, replaceFragment: Boolean, addToBackStack: Boolean) {
        startFragment(fragment, R.id.fragmentContainer, replaceFragment, addToBackStack, "")
    }

    fun startFragment(fragment: Fragment, container: Int, replaceFragment: Boolean, addToBackStack: Boolean, tag: String) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        if (replaceFragment) {
            if (!TextUtils.isEmpty(tag)) {
                fragmentTransaction.replace(container, fragment, tag)
            } else {
                fragmentTransaction.replace(container, fragment)
            }
        } else {
            if (!TextUtils.isEmpty(tag)) {
                fragmentTransaction.add(container, fragment, tag)
            } else {
                fragmentTransaction.add(container, fragment)
            }
        }
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            Log.d("x-class:Fragment", fragment.javaClass.simpleName)
        }
        commitFragment(fragmentTransaction)
    }

    private fun commitFragment(fragmentTransaction: FragmentTransaction?): Boolean {
        if (fragmentTransaction != null) {
            try {
                fragmentTransaction.commit()
                return true
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }

        }
        return false
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentDispatchingAndroidInjector
    }
}
