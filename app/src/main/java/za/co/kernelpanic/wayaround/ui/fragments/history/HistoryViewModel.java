package za.co.kernelpanic.wayaround.ui.fragments.history;

import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import za.co.kernelpanic.wayaround.repository.WayAroundRepositoryImpl;

public class HistoryViewModel extends ViewModel {

    private WayAroundRepositoryImpl wayAroundRepository;

    @Inject
    public HistoryViewModel(WayAroundRepositoryImpl wayAroundRepository) {
        this.wayAroundRepository = wayAroundRepository;
    }
}
