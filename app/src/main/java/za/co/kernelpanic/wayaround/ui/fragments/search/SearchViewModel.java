package za.co.kernelpanic.wayaround.ui.fragments.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats;
import za.co.kernelpanic.wayaround.repository.WayAroundRepositoryImpl;

@Singleton
public class SearchViewModel extends ViewModel {

    private WayAroundRepositoryImpl wayAroundRepository;

    @Inject
    public SearchViewModel(WayAroundRepositoryImpl wayAroundRepository) {
        this.wayAroundRepository = wayAroundRepository;
    }

    public LiveData<TrainStats> getTrainStats() {
        return wayAroundRepository.getTrainStats();
    }
}
