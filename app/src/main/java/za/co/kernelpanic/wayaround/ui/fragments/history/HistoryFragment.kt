package za.co.kernelpanic.wayaround.ui.fragments.history

import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import javax.inject.Inject

import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import za.co.kernelpanic.wayaround.R
import za.co.kernelpanic.wayaround.di.modules.WayAroundViewModelFactory

class HistoryFragment : DaggerFragment() {

    @Inject
    lateinit var wayAroundViewModelFactory: WayAroundViewModelFactory

    @Inject
    lateinit var historyViewModel: HistoryViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        historyViewModel = ViewModelProviders.of(this, wayAroundViewModelFactory).get(HistoryViewModel::class.java)
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.history_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {

        fun newInstance(): HistoryFragment {
            return HistoryFragment()
        }
    }
}// Required empty public constructor
