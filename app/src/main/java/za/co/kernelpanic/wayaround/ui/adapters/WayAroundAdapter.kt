package za.co.kernelpanic.wayaround.ui.adapters

import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import za.co.kernelpanic.wayaround.data.model.london.underground.OnlineRoutes
import za.co.kernelpanic.wayaround.data.model.london.underground.WarningRoutes
import za.co.kernelpanic.wayaround.databinding.OnlineRoutesRecyclerviewLayoutBinding
import za.co.kernelpanic.wayaround.databinding.WarningRoutesRecyclerviewLayoutBinding

class WayAroundAdapter(private val onlineRoutes: List<OnlineRoutes>, private val warningRoutes: List<WarningRoutes>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        
        return when(viewType) {
            TYPE_ONLINE -> {
                val binding = OnlineRoutesRecyclerviewLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                WayAroundOnlineViewHolder(binding)
            }
            TYPE_WARNING -> {
                val binding = WarningRoutesRecyclerviewLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                WayAroundWarningViewHolder(binding)
            }
            else -> {}

        }
    }

    override fun getItemCount(): Int {
        return -1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    companion object {
        private const val TYPE_ONLINE = 1
        private const val TYPE_WARNING = 2
    }
}