package za.co.kernelpanic.wayaround.ui.activity

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.Fragment
import android.view.MenuItem

import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import za.co.kernelpanic.wayaround.R
import za.co.kernelpanic.wayaround.databinding.UndergroundActivityBinding
import za.co.kernelpanic.wayaround.ui.fragments.history.HistoryFragment
import za.co.kernelpanic.wayaround.ui.fragments.search.SearchFragment
import za.co.kernelpanic.wayaround.ui.fragments.status.StatusFragment

class UndergroundActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: UndergroundActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.underground_activity)
        initViews()
    }

    private fun initViews() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {

        when (menuItem.itemId) {

            R.id.status -> {
                val statusFragment = StatusFragment.newInstance()
                startFragment(statusFragment, replaceFragment = true, addToBackStack = false)
                return true
            }

            R.id.history -> {
                val historyFragment = HistoryFragment.newInstance()
                startFragment(historyFragment, replaceFragment = true, addToBackStack = false)
                return true
            }

            R.id.lookup -> {
                val searchFragment = SearchFragment.newInstance()
                startFragment(searchFragment, replaceFragment = true, addToBackStack = false)
                return true
            }
        }
        return false
    }
}
