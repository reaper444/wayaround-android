package za.co.kernelpanic.wayaround.di.components

import android.app.Application

import javax.inject.Singleton

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import za.co.kernelpanic.wayaround.WayAroundApp
import za.co.kernelpanic.wayaround.di.modules.ActivityModule
import za.co.kernelpanic.wayaround.di.modules.AppModule
import za.co.kernelpanic.wayaround.di.modules.NetworkModule
import za.co.kernelpanic.wayaround.di.modules.ViewModelModule

@Singleton
@Component(modules = [AndroidInjectionModule::class, NetworkModule::class, AppModule::class, ViewModelModule::class, ActivityModule::class, AndroidSupportInjectionModule::class])
interface AppComponent {

    fun myapp(): Application

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(target: WayAroundApp)
}
