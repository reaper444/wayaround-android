package za.co.kernelpanic.wayaround.repository;

import androidx.lifecycle.LiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import za.co.kernelpanic.wayaround.data.model.london.underground.TrainStats;
import za.co.kernelpanic.wayaround.repository.remote.NetworkDataSource;
import za.co.kernelpanic.wayaround.repository.remote.NetworkDataSourceImpl;

@Singleton
public class WayAroundRepositoryImpl implements WayAroundRepository {

    private NetworkDataSource networkDataSource;

    @Inject
    public WayAroundRepositoryImpl(NetworkDataSourceImpl networkDataSource){
        this.networkDataSource = networkDataSource;
    }

    @Override
    public LiveData<TrainStats> getTrainStats() {
        return networkDataSource.getStatus();
    }
}
