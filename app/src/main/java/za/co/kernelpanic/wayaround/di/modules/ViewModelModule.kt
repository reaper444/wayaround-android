package za.co.kernelpanic.wayaround.di.modules

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.multibindings.IntoMap
import za.co.kernelpanic.wayaround.ui.fragments.history.HistoryViewModel
import za.co.kernelpanic.wayaround.ui.fragments.search.SearchViewModel
import za.co.kernelpanic.wayaround.ui.fragments.status.StatusViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HistoryViewModel::class)
    internal abstract fun providesHistoryViewModel(historyViewModel: HistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    internal abstract fun providesSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StatusViewModel::class)
    internal abstract fun providesStatusViewModel(statusViewModel: StatusViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(wayAroundViewModelFactory: WayAroundViewModelFactory): ViewModelProvider.Factory
}