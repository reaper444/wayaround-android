package za.co.kernelpanic.wayaround.data.model.london.underground

import com.squareup.moshi.Json

data class Underground(@Json(name = "online") var trainsOnline: List<OnlineRoutes> = listOf(),
                       @Json(name = "warn") var trainsWarning: List<WarningRoutes> = listOf())