package za.co.kernelpanic.wayaround.ui.fragments.status

import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import javax.inject.Inject

import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import za.co.kernelpanic.wayaround.R
import za.co.kernelpanic.wayaround.data.model.london.underground.BaseData
import za.co.kernelpanic.wayaround.databinding.StatusFragmentBinding
import za.co.kernelpanic.wayaround.di.modules.WayAroundViewModelFactory
import za.co.kernelpanic.wayaround.ui.adapters.CommonUtils
import za.co.kernelpanic.wayaround.ui.adapters.WayAroundAdapter

class StatusFragment : DaggerFragment() {

    private lateinit var binding: StatusFragmentBinding

    @Inject
    lateinit var wayAroundViewModelFactory: WayAroundViewModelFactory

    @Inject
    lateinit var statusViewModel: StatusViewModel

    private lateinit var recyclerView: RecyclerView

    private lateinit var wayAroundAdapter: WayAroundAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        statusViewModel = ViewModelProviders.of(this, wayAroundViewModelFactory).get(StatusViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.status_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.statusInfoRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = DefaultItemAnimator()
        statusViewModel.trainData.observe(this, Observer {
            if (it != null) {
                wayAroundAdapter = if (CommonUtils.isUndergroundWarningAvailable(it.underground.trainsWarning)) {
                    val trainStats = CommonUtils.mergeUndergroundRouteLists(it.underground.trainsOnline, it.underground.trainsWarning)
                    WayAroundAdapter(trainStats)
                } else {
                    WayAroundAdapter(it.underground.trainsOnline)
                }
                recyclerView.adapter = wayAroundAdapter
            }
        })
    }

    companion object {
        fun newInstance(): StatusFragment {
            return StatusFragment()
        }
    }
}