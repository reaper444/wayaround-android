package za.co.kernelpanic.wayaround.ui.adapters

import androidx.recyclerview.widget.RecyclerView
import za.co.kernelpanic.wayaround.databinding.OnlineRoutesRecyclerviewLayoutBinding
import za.co.kernelpanic.wayaround.databinding.WarningRoutesRecyclerviewLayoutBinding

class WayAroundOnlineViewHolder(val binding: OnlineRoutesRecyclerviewLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind() {
        binding.executePendingBindings()
    }
}

class WayAroundWarningViewHolder(val binding: WarningRoutesRecyclerviewLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind() {
        binding.executePendingBindings()
    }
}