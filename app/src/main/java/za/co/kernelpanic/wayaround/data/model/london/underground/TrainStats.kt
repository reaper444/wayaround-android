package za.co.kernelpanic.wayaround.data.model.london.underground

import com.squareup.moshi.Json

data class TrainStats (@Json(name = "underground") var underground: Underground)